import pandas as pd
from nltk.tokenize import TweetTokenizer
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
# Read data
df = pd.read_csv("data.csv", delimiter=",", encoding='latin-1', nrows = 1000)

# df = df.drop(["Date", "ID", "Query"], 1)

# Trimming data
df.drop(["Date", "ID", "Query", "User"], 1, inplace=True)

# Declare Tokenizer
toknizr = TweetTokenizer(strip_handles=True, preserve_case=False, reduce_len=True)

# CONSIDER LEMMATIZATION INSTEAD OF STEMMING
# Declare Stemmer
stemmer = SnowballStemmer("english", ignore_stopwords=True)

toknizedTweets = list()
for each in df["Tweet"]:
    strippedTweet = toknizr.tokenize(each)
    FinalTweet = list()
    for word in strippedTweet:
        if len(word) < 2 or word in stopwords.words('english') or word.find("http://") != -1:
            continue
        else:
            FinalTweet.append(stemmer.stem(word))

    toknizedTweets.append(FinalTweet)

print(toknizedTweets)

